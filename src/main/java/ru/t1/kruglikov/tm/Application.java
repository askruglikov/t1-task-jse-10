package ru.t1.kruglikov.tm;

import ru.t1.kruglikov.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }
    
}
