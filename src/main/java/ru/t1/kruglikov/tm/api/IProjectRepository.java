package ru.t1.kruglikov.tm.api;

import ru.t1.kruglikov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void clear();

    List<Project> findAll();

}
