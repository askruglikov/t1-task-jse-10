package ru.t1.kruglikov.tm.repository;

import ru.t1.kruglikov.tm.api.ICommandRepository;
import ru.t1.kruglikov.tm.constant.ArgumentConst;
import ru.t1.kruglikov.tm.constant.CommandConst;
import ru.t1.kruglikov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show help.");
    public static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show version.");
    public static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show about developer.");
    public static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system information.");
    public static final Command ARGUMENT = new Command(CommandConst.ARGUMENT, ArgumentConst.ARGUMENT, "Show argument list.");
    public static final Command COMMAND = new Command(CommandConst.COMMAND, ArgumentConst.COMMAND, "Show command list.");
    public static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");
    public static final Command PROJECT_LIST = new Command(CommandConst.PROJECT_LIST, null, "Show projects list.");
    public static final Command PROJECT_CREATE = new Command(CommandConst.PROJECT_CREATE, null, "Create new project.");
    public static final Command PROJECT_CLEAR = new Command(CommandConst.PROJECT_CLEAR, null, "Remove all projects.");
    public static final Command TASK_LIST = new Command(CommandConst.TASK_LIST, null, "Show tasks list.");
    public static final Command TASK_CREATE = new Command(CommandConst.TASK_CREATE, null, "Create new task.");
    public static final Command TASK_CLEAR = new Command(CommandConst.TASK_CLEAR, null, "Remove all tasks.");

    public static final Command[] TERMINAL_COMMANDS = {
            HELP, VERSION, ABOUT, INFO, ARGUMENT, COMMAND, EXIT,PROJECT_LIST,
            PROJECT_CREATE,PROJECT_CLEAR,TASK_LIST,TASK_CREATE,TASK_CLEAR
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
