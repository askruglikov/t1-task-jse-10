package ru.t1.kruglikov.tm.util;

import java.util.Scanner;

public interface TerminalUtil {
    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }
}
