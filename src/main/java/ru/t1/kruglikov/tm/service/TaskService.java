package ru.t1.kruglikov.tm.service;


import ru.t1.kruglikov.tm.api.ITaskRepository;
import ru.t1.kruglikov.tm.api.ITaskService;
import ru.t1.kruglikov.tm.model.Task;

import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name, final String description) {
        if (name==null || name.isEmpty()) return null;
        if (description==null) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @Override
    public void add(Task project) {
        if (project == null) return;
        taskRepository.add(project);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

}
